def student(name, age, rno):
    print("name=",name)
    print("age=",age)
    print("roll no.=",rno)
    print("-------------------------")

#Positional Arguments
student("Aman",22,1)
#Keyword Arguments
student(age=22,name="peter",rno=2)
student("James", rno=3, age=21)