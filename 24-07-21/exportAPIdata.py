# pip install requests
import requests

data = requests.get("https://restcountries.eu/rest/v2/all").json()
file = open("countries.csv","w")
file.write("Name,Population,Flag URL,Area\n")
r=""
htm=""
for i in data:
    r+="{},{},{},{}\n".format(
        i['name'],i['population'],i['flag'],i["area"]
    )
    htm+="<div style='width:29%;padding:1%;margin:1%;float:left;box-shadow:0px 0px 10px gray;'><img src='{}' style='height:200px;width:100%;'><h1>{}</h1></div>".format(i["flag"],i["name"])
file.write(r)

with open("countries.html","w") as f:
    f.write(htm)
print("data exported successfully!")
# print(data)