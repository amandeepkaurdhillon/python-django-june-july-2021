#open a file 
# file = open("notes.txt","r")
file = open("notes.txt","rb")

#read file content 
# print(file.read())
# print(file.read(6))
# print(file.readline())
# print(file.readline())
# print(file.readline())

# lines = file.readlines()
# print(lines[0])

data = file.read()
print(data)
print(type(data))

#close a file 
file.close()