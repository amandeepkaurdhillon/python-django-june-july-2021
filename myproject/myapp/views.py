from django.shortcuts import render
from django.http import HttpResponse
import requests
# Create your views here.

def index(request):
    return HttpResponse("<h1>This is my first django view</h1>")

def first(request):
    colors = ["red","green","blue","magenta","rgb(222,34,38)","black"]
    a = "I am from python views"
    return render(request, "first.html", {"col":colors,"a":a})

def all_countries(request):
    API = "https://restcountries.eu/rest/v2/all" 
    data = requests.get(API).json()
    return render(request, "countries.html", {"countries":data})