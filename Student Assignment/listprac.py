ls = []

print("press 1: To add element")
print("press 2: View element")
print("press 3: Remove element")
print("press 4: Sum")
print("press 5: Multiply")
print("press 0: Exit")
print("  ")
while True:
    index = int(input("Enter choice: "))
    if index == 1:
        num = int(input("Enter Number to add: "))
        ls.append(num)
        print(str(num) + " added successfully!")
        print("")
    elif index == 2:
        print("Total: " + str(len(ls)))
        print(ls)
        print("")
    elif index == 3:
        rem = int(input("Enter number to Remove: "))
        val = ls.index(rem)
        ls.pop(val)
        print(str(rem)+" removed successfully!")
        print("")
    elif index == 4:
        sump = sum(ls)
        print("Total Sum: " + str(sump))
        print("")
    elif index == 5:
        result = 1
        for x in ls:
            result = result * x
        print("Multiplication: " + str(result))
        print("")
    elif index == 0:
        break
