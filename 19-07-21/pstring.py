a = "I am string in python"
print(a)
print(len(a))
## Indexing 
# print(a[0])
# print(a[2])
# print(a[-1])

## Slicing
print(a[2:])
print(a[:3])
print(a[3:8])

##Stepping
a = "0123456789"
print(a[::1])
print(a[::2])
print(a[::3])
print(a[::-1])

#Membership
print("1" in a)
print("1" not in a)
print("*" not in a)

# COncatenation
print(a+"hello")

print("ab"*4) #iteration
