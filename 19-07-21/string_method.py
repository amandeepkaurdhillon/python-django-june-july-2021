intro = "Python was created by guido van rossum"
# print(dir(intro))

print(intro.upper())
print(intro.lower())
print(intro.title())
print(intro.capitalize())
print(intro.swapcase())

a = "ABCD"
# a = "9876"
print(a.isupper())
print(a.islower())
print(a.isdigit())


st = "apple cherry mango orange"
print(st[0])
ls = st.split(" ")
print(ls)
print(ls[0])

