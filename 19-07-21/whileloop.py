#WAP to print 1 to 10 using while loop 

# i=1 #initialization
# while i<=10: #condition
#     print(i) #body
#     i = i+1

i = 10
while i>=1:
    print(i)
    i-=1