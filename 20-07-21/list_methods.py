days = ["Monday", "Tuesday", "Wednesday","Monday"]
print('before: ',days)
# print(dir(days))

# ##update
# days[0] = "Sunday"
# ##insert
# days.append("Thursday")
##delete
# days.pop()
# days.pop(1)
# days.remove("Tuesday")
c = days.copy()
days.clear()
print('after: ', days)
print("c=",c)
print(c.index("Monday"))
print(c.count("Monday"))

ls = ["Saturday","Sunday"]
# c.append(ls)
c.extend(ls)
print(c[5])
# print(c[4][1])

num = [20,4,33,230,443,1]
num.sort()
num.reverse()
print(num)