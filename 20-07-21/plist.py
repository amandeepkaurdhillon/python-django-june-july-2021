colors = ["orange","magenta","purple","pink"]
primary = ["red", "green", "blue"]

print(type(colors))

#Indexing 
print(colors[0])
print(colors[-1])

#Slicing
print(colors[1:])

#stepping
print(colors[::2])
print(colors[::-1])

#concatenation
print(colors+primary)

#iteration
print(primary*3)

#membership
print("red" in primary)
print("red" not in primary)