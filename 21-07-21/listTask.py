options = """
    Press 1: To add Number
    Press 2: To view Number
    Press 3: To Remove Number
    Press 4: To Sum
    Press 5: To Multiply
    Press 0: To exit
"""
print(options)

all = []
while True:
    op= input("Enter your choice: ")
    if op=="1":
        num = int(input("Enter Number to add: "))
        all.append(num)
        print("{} added successfully!\n".format(num))

    elif op=="2":
        print("Total: ", len(all))
        c=1
        for i in all:
            print("{}. {}".format(c,i))
            c+=1
        # for c,n in enumerate(all,start=1):
        #     print("{}. {}".format(c,n))
        print()
    elif op=="3":
        to_remove = int(input("Enter Number to remove: "))
        if to_remove in all:
            all.remove(to_remove)
            print("{} removed successfully!\n".format(to_remove))
        else:
            print("{} not found in list!\n".format(to_remove))

    elif op=="4":
        print("Sum: {}\n".format(sum(all)))

    elif op=="5":
        s = 1
        for i in all:
            s = s*i
        print("Multiplication: ", s,"\n")
    elif op=="0":
        break
    else:
        print("Invalid Choice!\n")