colors = ("red","orange","black","magenta")
b = ("white","cyan")

#Membership
print("red" in colors)
print("red" not in colors)

#Concatenation
print(colors+b)
print(b*3)

print(colors.count("red"))
print(colors.count("green"))
print(colors.index("magenta"))
# print(dir(b))
# b[0]="hello"
# del b[1]
