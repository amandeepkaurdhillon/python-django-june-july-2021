movie = {
    "name":"PK",
    "actor":"Amir Khan",
    "DOR":"10 Sept. 2014",
    "revenue":"200 Crore"
}
print("Before: ", movie)

# #Insert/Update Element
# movie["actress"] = "Anushka"
# movie["DOR"] = "2012"
# movie.update({'actor':'Amir','production':'XYZ'})

##Remove elements
# movie.pop("DOR")
# movie.popitem()
# movie.popitem()
# print(dir(movie))

# print(movie['nameh'])
# print(movie.get('nameh'))
copy = movie.copy()
movie.clear()
print("After: ",movie)
print("Copy=",copy)

employee = ['name','age','salary','experience']
print({}.fromkeys(employee, "N/A"))