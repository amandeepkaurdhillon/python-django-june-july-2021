a = 10,20,30,40

print(a)
# print(type(a))

#Indexing
print(a[0])
print(a[-1])
#slicing
print(a[1:])
print(a[:2])
print(a[1:3])
#Stepping
print(a[::2])
print(a[::-1])