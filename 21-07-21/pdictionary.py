student = {
    "name":"Peter Parker",
    "rno":1,
    "hobbies":["Playing","Reading Books","Travelling"],
    "tech":{
        "front-end":["HTML","CSS","JS"],
        "back-end":["Python","Django","SQLite"]
    }
}

# # print(student)
# ## Access Elements 
# print(student['name'])
# print(student["hobbies"][2])
# print(student["tech"]["back-end"][-1])

## Looping

# for i in student:
#     print(i,"=", student[i])

# val = student.values()
# for k in val:
#     print(k)

# keys = student.keys()
# for k in keys:
#     print(k)

item = student.items()
for key,value in item:
    print("{} => {}".format(key, value))