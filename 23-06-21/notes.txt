CSS Margin = Element's outside space
CSS Padding  = Element & Border's inside space 

Equal on all side 
    margin:100px;
    padding:100px;

Only Single side 
    margin-left, margin-right, margin-top, margin-bottom
    padding-left,...

Two sides equal 
    margin:vertical horizontal;
    padding:vertical horizontal;

All sides Unequal
    margin:top right bottom left;
    padding:top right bottom left;

HTML Links (a)
    href = path to visit 
    target = where to open 
        _self, _blank