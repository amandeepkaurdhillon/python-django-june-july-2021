class Main:
    pass
class Student(Main):
    college = "ABCD Engg. College"
    def __init__(self, name="Peter", age=20):
        self.name = name 
        self.age = age
    def profile(self):
        print("{} is {} years old!".format(self.name, self.age))

def Demo(Main):
    pass
def Abcd(Student, Demo):
    pass 
#simple Inheritance
class Account(Student):
    college = "XYZ College of management"
    def fee_details(self, total, paid):
        print("College Name: ", super().college)
        print("Here are fee details of {}".format(self.name))
        print("Total Fee: Rs.{}/-".format(total))
        print("Paid Fee: Rs.{}/-".format(paid))
        print("Pending Fee: Rs.{}/-".format(total-paid))

class Library(Student):
    pass
s1 = Account()
# print("college=",s1.college)
s1.fee_details(10000,2030)