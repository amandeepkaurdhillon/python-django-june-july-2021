class MyError(Exception):
    def __init__(self, val="first number must be greater"):
        self.v = val 
    def __str__(self):
        return self.v 
try:
    x = int(input("Enter First Number: "))
    y = int(input("Enter Second Number: "))
    if x>y:
        print("Result: ",x-y)
    else:
        raise MyError
except MyError as err:
    print("Something went wrong!")
    print(err)
    
# print("hello there") 
# raise NameError