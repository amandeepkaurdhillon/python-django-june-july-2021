try:
    a = "hello there"
    print(a)
    # print(a+10)
except:
    print("Except part, error occurred!")

else:
    print("Else part!")

finally:
    print("Finally, I will execute always!")
