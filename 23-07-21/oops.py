#create a class
class Student:
    #data member
    clz = "ABCD Engg. College"
    #member function 
    def register(self):
        print(" I am a member function")

# Creat an object 
s1 = Student()
print("Before: ",s1.clz)
#Update an object
s1.clz="XYZ"
print("After: ", s1.clz)
s1.register()