class Employee:
    comp_name = "ABCD Tech" #public d.m.
    __pin=123456 #private d.m.
    
    def __profile(self):
        print("This is private member function")
    def greet(self): #public m.f.
        self.__profile()
        print("Good Evening all!", self.__pin)

obj = Employee()
print(obj.comp_name)
obj.greet()
## Object can't use private  data members or member functions
# print(obj.__pin)
# obj.__profile()