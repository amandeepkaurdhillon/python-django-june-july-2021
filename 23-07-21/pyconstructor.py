class Circle:
    pi = 3.14
    def __init__(self,radius): #constructor
        self.r = radius
        return None
    def cal_area(self):
        print("Radius:{} Area:{}".format(self.r, self.pi*self.r**2))
    def __del__(self):
        del self.r 

c1 = Circle(29)
c2 = Circle(9)
c3 = Circle(10)

c1.cal_area()
c3.cal_area()