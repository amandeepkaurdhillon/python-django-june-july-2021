class Circle:
    pi = 3.14 

    def set_radius(self,radius):
        self.r = radius
        print("Radius Set!")

    def cal_area(self):
        print("Radius:{} Area:{}".format(self.r, self.pi*self.r**2))

c1 = Circle()
c1.set_radius(20)
c2 = Circle()
c2.set_radius(10)
c3 = Circle()
c3.set_radius(25)

c2.cal_area()
c1.cal_area()
c3.cal_area()