BS Colors 
    (bg-, text-)
    success 
    info 
    warning
    danger 
    light 
    dark 
    primary 
    secondary


BS MARGIN PADDING 

1rem = 16px 
All sides equal  (up to 5)
    m-1 p-1
Single side 
    mt-, mr-, mb-, ml- 
    pt-, pr-, pb-, pl-
Two side equal 
    py-1, my-1 
    px-1, mx-1
    